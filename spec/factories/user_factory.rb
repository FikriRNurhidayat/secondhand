FactoryBot.define do
  factory(:user) do
    name { Faker::Games::Dota.hero }
    email { Faker::Internet.email }
    password { Faker::Internet.password }
    phone_number { Faker::PhoneNumber.cell_phone_in_e164 }
    address { Faker::Address.full_address }
    city { City.all.sample || create(:city) }
    avatar do
      Rack::Test::UploadedFile.new(Rails.root.join('db/assets/avatar.png'), 'image/png')
    end
  end
end
