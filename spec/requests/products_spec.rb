require 'swagger_helper'

RSpec.describe 'Products API' do
  path '/products.json' do
    post 'Create Product' do
      tags 'Products'
      consumes 'multipart/form-data'
      security [ bearer_auth: [] ]
      parameter name: :product, in: :formData, schema: {
        type: :object,
        properties: {
          'product[name]': {
            type: :string,
            example: Faker::Games::Dota.item,
          },
          'product[price]': {
            type: :string,
            example: Faker::Commerce.price(range: 1000..200_000),
          },
          'product[description]': {
            type: :string,
            example: Faker::Lorem.paragraph,
          },
          'product[status]': {
            type: :string,
            enum: ['draft', 'published'],
          },
          'product[category_id]': {
            type: :integer,
            example: 1,
          },
          'product[images]': {
            type: :array,
            explode: true,
            items: {
              type: :string,
              format: :binary,
            }
          },
        },
        required: [ 'product[name]', 'product[price]', 'product[description]', 'product[status]', 'product[category_id]', 'product[images]' ]
      }

      response '201', 'Product created' do
        let(:Authorization) { bearer_auth }
        let(:category) { create(:category) }
        let(:product) {
          {
            name: Faker::Games::Dota.item,
            price: Faker::Commerce.price(range: 1000..200_000),
            description: Faker::Lorem.paragraph,
            status: 'draft',
            category_id: category.id,
            images: Dir[Rails.root.join('db/assets/sample*.png')].map { |path| fixture_file_upload(path, 'image/png') },
          }
        }

        after do |example|
          content = example.metadata[:response][:content] || {}
          example_spec = {
            "application/json"=>{
              examples: {
                ok: {
                  value: JSON.parse(response.body, symbolize_names: true)
                }
              }
            }
          }

          example.metadata[:response][:content] = content.deep_merge(example_spec)
        end

        run_test!
      end
    end

    get 'List Products' do
      tags 'Products'
      security [ {}, bearer_auth: [], ]
      parameter name: :page, in: :query, schema: {
        type: :number,
        format: :integer,
        required: false,
        example: 1
      }
      parameter name: :product, in: :query, schema: {
        type: :object,
        required: false,
        properties: {
          category_id: {
            type: :number,
            format: :integer,
            example: 1
          },
          user_id: {
            type: :number,
            format: :integer,
            example: 1
          }
        },
      }

      response '200', 'Product listed' do
        let(:category) { create(:category) }
        let(:page) { 1 }
        let(:product) { { category_id: category.id } }

        let!(:products) { create_list(:product, 20, user: user) }

        after do |example|
          content = example.metadata[:response][:content] || {}
          example_spec = {
            "application/json"=>{
              examples: {
                ok: {
                  value: JSON.parse(response.body, symbolize_names: true)
                }
              }
            }
          }

          example.metadata[:response][:content] = content.deep_merge(example_spec)
        end

        run_test!
      end
    end
  end

  path '/products/{id}.json' do
    get 'Get Product' do
      tags 'Products'
      security [ {}, bearer_auth: [] ]
      parameter name: :id, in: :path, type: :string

      response '200', 'Product retrieved' do
        let(:Authorization) { bearer_auth }
        let(:category) { create(:category) }
        let(:id) { create(:product).id }

        after do |example|
          content = example.metadata[:response][:content] || {}
          example_spec = {
            "application/json"=>{
              examples: {
                ok: {
                  value: JSON.parse(response.body, symbolize_names: true)
                }
              }
            }
          }

          example.metadata[:response][:content] = content.deep_merge(example_spec)
        end

        run_test!
      end
    end

    put 'Update Product' do
      tags 'Products'
      security [ bearer_auth: [] ]
      consumes 'multipart/form-data'
      parameter name: :id, in: :path, type: :number
      parameter name: :product, in: :formData, schema: {
        type: :object,
        properties: {
          'product[name]': {
            type: :string,
            example: Faker::Games::Dota.item,
          },
          'product[price]': {
            type: :string,
            example: Faker::Commerce.price(range: 1000..200_000),
          },
          'product[description]': {
            type: :string,
            example: Faker::Lorem.paragraph,
          },
          'product[status]': {
            type: :string,
            enum: ['draft', 'published'],
          },
          'product[category_id]': {
            type: :integer,
            example: 1,
          },
          'product[images]': {
            type: :array,
            explode: true,
            items: {
              type: :string,
              format: :binary,
            }
          },
          'product[persisted_images]': {
            type: :array,
            explode: true,
            items: {
              type: :string,
            },
            example: [
              "eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBb01DIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--3a65eea97a596a39fdb3fa84812868b6e8feaf16",
              "eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBb1FDIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--fb121120f09f458f8ba59bcd300750cbd64a719d",
            ],
          },
        },
      }

      response '200', 'Product updated' do
        let(:Authorization) { bearer_auth }
        let(:category) { create(:category) }
        let(:record) { create(:product, user: user) }
        let(:id) { record.id }
        let(:product) {
          {
            name: Faker::Games::Dota.item,
            price: Faker::Commerce.price(range: 1000..200_000),
            description: Faker::Lorem.paragraph,
            status: 'draft',
            category_id: category.id,
            persisted_images: record.images.map(&:signed_id),
            images: Dir[Rails.root.join('db/assets/sample*.png')].map { |path| fixture_file_upload(path, 'image/png') },
          }
        }

        after do |example|
          content = example.metadata[:response][:content] || {}
          example_spec = {
            "application/json"=>{
              examples: {
                ok: {
                  value: JSON.parse(response.body, symbolize_names: true)
                }
              }
            }
          }

          example.metadata[:response][:content] = content.deep_merge(example_spec)
        end

        run_test!
      end
    end

    delete 'Delete Product' do
      tags 'Products'
      security [ bearer_auth: [] ]
      consumes 'multipart/form-data'
      parameter name: :id, in: :path, type: :number

      response '204', 'Product deleted' do
        let(:Authorization) { bearer_auth }
        let(:category) { create(:category) }
        let(:product) { create(:product, user: user) }
        let(:id) { product.id }
        run_test!
      end
    end
  end
end
