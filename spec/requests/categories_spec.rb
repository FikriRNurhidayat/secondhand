require 'swagger_helper'

RSpec.describe 'Categories API' do
  path '/categories.json' do
    get 'List Categories' do
      tags 'Categories'

      response '200', 'Category Listed' do
        let!(:categories) { create_list(:category, 5) }

        after do |example|
          content = example.metadata[:response][:content] || {}
          example_spec = {
            "application/json"=>{
              examples: {
                ok: {
                  value: JSON.parse(response.body, symbolize_names: true)
                }
              }
            }
          }

          example.metadata[:response][:content] = content.deep_merge(example_spec)
        end

        run_test!
      end
    end
  end

  path '/categories/{id}.json' do
    get 'Get Category' do
      tags 'Categories'
      parameter name: :id, in: :path, type: :string

      response '200', 'Category Retrieved' do
        let(:category) { create(:category) }
        let(:id) { category.id }

        after do |example|
          content = example.metadata[:response][:content] || {}
          example_spec = {
            "application/json"=>{
              examples: {
                ok: {
                  value: JSON.parse(response.body, symbolize_names: true)
                }
              }
            }
          }

          example.metadata[:response][:content] = content.deep_merge(example_spec)
        end

        run_test!
      end
    end
  end
end
