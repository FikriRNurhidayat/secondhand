# frozen_string_literal: true

class Offer < ApplicationRecord
  belongs_to :from, class_name: User.name
  belongs_to :to,   class_name: User.name
  belongs_to :product

  enum status: {
    initiated: 'INITIATED',
    accepted: 'ACCEPTED',
    denied: 'DENIED',
    finished: 'FINISHED',
    cancelled: 'CANCELLED'
  }

  scope :related_to, lambda { |user|
    where('to_id = :user_id OR from_id = :user_id', user_id: user.id)
  }

  def price_in_format
    "Rp #{price.to_i.to_fs(:delimited, delimiter: '.')}"
  end

  def notify_user
    notification = Notification.new(subject: 'Penawaran produk',
                                    body: "#{product.name}<br>#{product.price_in_format}<br>Ditawar #{price_in_format}", click_action: Rails.application.routes.url_helpers.user_offers_path(from_id), user: to)

    unless notification.save
      self.errors = notification.errors
      return false
    end

    notification.image.attach(product.images.first.blob) if product.images.attached?

    true
  end
end
