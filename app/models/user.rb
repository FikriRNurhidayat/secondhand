# frozen_string_literal: true

class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :jwt_authenticatable, jwt_revocation_strategy: JwtDenylist

  validates_presence_of   :name
  validates_uniqueness_of :email

  belongs_to :city, optional: true
  has_many :notifications

  has_one_attached :avatar

  def nickname
    name.try(:split, ' ').try(:first)
  end
end
