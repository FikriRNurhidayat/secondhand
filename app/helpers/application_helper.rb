# frozen_string_literal: true

module ApplicationHelper
  def active_class(path)
    return 'active' if request.path == path

    ''
  end

  def current_token
    request.env['warden-jwt_auth.token']
  end

  def search_term
    params[:q].present? ? params[:q] : ""
  end

  def notifications
    current_user.notifications.with_attached_image.order(created_at: :desc).paginate(page: 1, per_page: 10)
  end

  def unread_notification?
    current_user.notifications.unread.exists?
  end

  def to_price(price)
    "Rp #{number_with_delimiter(price.to_i, delimiter: '.')}"
  end
  
  def last_notifications?(index, notifications)
    index.eql?(notifications.length - 1)
  end

  def field_error?(resource, field)
    resource.errors.any? && field_error(resource, field).present?
  end

  def field_error(resource, field)
    resource.errors.detect { |error| error.attribute.eql?(field) }
  end
end
