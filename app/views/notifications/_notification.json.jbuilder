json.extract! notification, :id, :subject, :body, :click_action, :read, :created_at, :updated_at
json.image do
  json.filename notification.image.filename
  json.url url_for(notification.image)
end
