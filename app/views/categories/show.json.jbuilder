# frozen_string_literal: true

json.category @category, partial: 'categories/category', as: :category
