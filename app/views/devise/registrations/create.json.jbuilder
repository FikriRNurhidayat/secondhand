json.user current_user, partial: 'users/user', as: :user
json.session do
  json.token current_token
end
