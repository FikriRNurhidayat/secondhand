# frozen_string_literal: true

json.extract! product, :id, :name, :price, :description, :category_id, :user_id, :created_at, :updated_at
json.url product_url(product, format: :json)

json.user product.user, partial: 'users/user', as: :user

json.images product.images do |attachment|
  json.id       attachment.signed_id
  json.filename attachment.filename
  json.url      url_for(attachment)
end
