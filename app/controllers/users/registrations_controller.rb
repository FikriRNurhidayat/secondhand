# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  before_action :configure_sign_up_params, only: [:create]

  protected

  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:name, :email, :password) }
  end

  def respond_with(resource, _opts = {})
    respond_to do |format|
      format.html do
        super
      end

      format.json do
        unless resource.persisted?
          @errors = resource.errors
          render "application/errors", status: :unauthorized
        else
          render "devise/registrations/create"
        end
      end
    end
  end
end
