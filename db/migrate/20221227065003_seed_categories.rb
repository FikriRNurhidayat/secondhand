# frozen_string_literal: true

class SeedCategories < ActiveRecord::Migration[7.0]
  def up
    Category.insert_all([
                          { name: 'Hobi' },
                          { name: 'Kendaraan' },
                          { name: 'Baju' },
                          { name: 'Elektronik' },
                          { name: 'Kesehatan' }
                        ])
  end

  def down
    Category.destroy_all
  end
end
